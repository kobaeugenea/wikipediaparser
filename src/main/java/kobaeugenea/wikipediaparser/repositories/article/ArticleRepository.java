package kobaeugenea.wikipediaparser.repositories.article;

import kobaeugenea.wikipediaparser.models.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository for Article entity
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article, UUID> {
}
