package kobaeugenea.wikipediaparser.services.article;

import kobaeugenea.wikipediaparser.models.Article;
import kobaeugenea.wikipediaparser.repositories.article.ArticleRepository;
import kobaeugenea.wikipediaparser.util.Guard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ArticleServiceImpl implements ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    @Transactional
    public void addArticle(Article article) {
        Guard.checkArgumentExists(article, "Article for DB saving shouldn't be null");
        articleRepository.save(article);
    }
}
