package kobaeugenea.wikipediaparser.services.article;

import kobaeugenea.wikipediaparser.models.Article;

/**
 * Interface for work with article entity
 */
public interface ArticleService {

    /**
     * Add article to database
     * @param article article entity
     */
    void addArticle(Article article);
}
