package kobaeugenea.wikipediaparser.services.parse;

import kobaeugenea.wikipediaparser.models.Article;

import java.io.IOException;

/**
 * Interface for Wikipedia parsing
 */
public interface ParseService {

    /**
     * Parse url and get article entity
     * @param url url for parsing
     * @return article entity
     */
    Article parserUrl(String url);
}
