package kobaeugenea.wikipediaparser.services.parse;

import kobaeugenea.wikipediaparser.models.Article;
import kobaeugenea.wikipediaparser.util.exception.ExceptionHelper;
import kobaeugenea.wikipediaparser.util.Guard;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class ParseServiceImpl implements ParseService {

    @Override
    public Article parserUrl(String url) {
        Guard.checkArgumentExists(url, "Url for parsing shouldn't be null");
        Document doc = ExceptionHelper.logAndExitWhenException(() -> Jsoup.connect(url).get(),
                                                               this.getClass().getName(),
                                                               "parserUrl");
        String articleName = doc.selectFirst("#firstHeading").text();
        byte[] articleImage = null;
        Elements image = doc.select(".infobox .image img");
        if(image.size() != 0){
            String imageSrc = image.first().absUrl("src");
            articleImage = ExceptionHelper.logAndExitWhenException(() -> Jsoup.connect(imageSrc).ignoreContentType(true).execute().bodyAsBytes(),
                                                                   this.getClass().getName(),
                                                                   "parserUrl");
        }
        return new Article(articleName, articleImage);
    }
}
