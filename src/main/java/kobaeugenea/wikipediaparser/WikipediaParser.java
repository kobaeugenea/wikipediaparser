package kobaeugenea.wikipediaparser;


import kobaeugenea.wikipediaparser.config.AppConfig;
import kobaeugenea.wikipediaparser.models.Article;
import kobaeugenea.wikipediaparser.services.article.ArticleService;
import kobaeugenea.wikipediaparser.services.parse.ParseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Wikipedia parser main class
 */
public class WikipediaParser {

	private final static Logger logger = LoggerFactory.getLogger(WikipediaParser.class);

	public static void main(String[] args) {
		if(args.length == 0){
			logger.error("Please, pass page url as first parameter to program");
			System.exit(0);
		}

		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		ArticleService articleService = context.getBean(ArticleService.class);
		ParseService parseService= context.getBean(ParseService.class);

		Article article = parseService.parserUrl(args[0]);
		articleService.addArticle(article);
	}
}
