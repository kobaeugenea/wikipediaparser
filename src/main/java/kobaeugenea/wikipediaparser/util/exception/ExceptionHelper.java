package kobaeugenea.wikipediaparser.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for working with exceptions
 */
public class ExceptionHelper {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHelper.class);

    /**
     * Execute function. Log and end program if function throws exception
     * @param function function for executing
     * @param className class name from which exception was thrown
     * @param methodName method from which exception was thrown
     * @return
     */
    public static <ResultType> ResultType logAndExitWhenException(SupplierWithException<ResultType> function,
                                                                  String className,
                                                                  String methodName){
        ResultType result = null;
        try {
            result = function.get();
        } catch (Throwable ex){
            logger.error("Exception in {}.{}() with cause = {} and exception {}",
                         className,
                         methodName,
                         ex.getCause(),
                         ex);
            System.exit(0);
        }
        return result;
    }
}
