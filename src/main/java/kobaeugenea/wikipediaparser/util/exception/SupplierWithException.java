package kobaeugenea.wikipediaparser.util.exception;

/**
 * Functional interface supplier that can throw exceptions
 */
@FunctionalInterface
public interface SupplierWithException<T> {

    T get() throws Throwable;
}
