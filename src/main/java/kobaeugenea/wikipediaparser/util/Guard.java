package kobaeugenea.wikipediaparser.util;

/**
 * Check valid class
 */
public class Guard {
    public Guard() {
    }

    public static void checkArgumentExists(Object argument, String errorInfo) {
        if (argument == null) {
            throw new IllegalArgumentException(errorInfo);
        }
    }
}
