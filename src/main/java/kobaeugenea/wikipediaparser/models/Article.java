package kobaeugenea.wikipediaparser.models;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Article entity
 */
@Entity
public class Article extends BaseEntity{

    @Column
    private String articleName;
    @Column
    private byte[] articleImage;

    public Article(String articleName, byte[] articleImage) {
        this.articleName = articleName;
        this.articleImage = articleImage;
    }

    public String getArticleName() {
        return articleName;
    }

    public byte[] getArticleImage() {
            return articleImage;
        }
}