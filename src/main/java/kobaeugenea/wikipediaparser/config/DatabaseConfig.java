package kobaeugenea.wikipediaparser.config;

import com.google.common.collect.ImmutableMap;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Config for database
 */
@Configuration
@EnableTransactionManagement()
@EnableJpaRepositories("kobaeugenea.wikipediaparser.repositories")
public class DatabaseConfig {

    @Bean(name = "dataSource")
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean(name = "hikariConfig")
    public HikariConfig hikariConfig_PostgreDev(@Value("${mysql.db.url}") String jdbcUrl,
                                                @Value("${mysql.db.username}") String username,
                                                @Value("${mysql.db.password}") String password) {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
        config.setJdbcUrl(jdbcUrl);
        config.setUsername(username);
        config.setPassword(password);
        return config;
    }


    @Bean(name = "jpaVendorAdapter")
    public JpaVendorAdapter jpaVendorAdapter_mysqlDev() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.MYSQL);
        vendorAdapter.setDatabasePlatform(org.hibernate.dialect.MySQL57Dialect.class.getName());
        vendorAdapter.setGenerateDdl(true);
        return vendorAdapter;
    }

    @Bean(name = "jdbcTemplate")
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "namedParameterJdbcTemplate")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean(name = "jpaDialect")
    public JpaDialect jpaDialect() {
        return new org.springframework.orm.jpa.vendor.HibernateJpaDialect();
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                                       JpaVendorAdapter jpaVendorAdapter,
                                                                       JpaDialect jpaDialect) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter);

        factoryBean.setJpaDialect(jpaDialect);
        factoryBean.setPackagesToScan("kobaeugenea.wikipediaparser.models");

        factoryBean.setJpaPropertyMap(ImmutableMap.of(AvailableSettings.ENABLE_LAZY_LOAD_NO_TRANS, true,
                                                      AvailableSettings.SHOW_SQL, false,
                                                      AvailableSettings.FORMAT_SQL, true,
                                                      "hibernate.connection.characterEncoding", "UTF-8",
                                                      "hibernate.hbm2ddl.auto", "update"));
        return factoryBean;
    }

    @Bean(name = "transactionManager")
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory,
                                                    DataSource dataSource,
                                                    JpaDialect jpaDialect) {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactory);
        jpaTransactionManager.setDataSource(dataSource);
        jpaTransactionManager.setJpaDialect(jpaDialect);
        return jpaTransactionManager;
    }
}
