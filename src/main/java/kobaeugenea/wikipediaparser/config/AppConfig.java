package kobaeugenea.wikipediaparser.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Application config
 */
@ComponentScan("kobaeugenea.wikipediaparser.services")
@PropertySources(@PropertySource(ignoreResourceNotFound = true, value = "classpath:db.properties"))
@Import(DatabaseConfig.class)
public class AppConfig {
}
